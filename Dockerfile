FROM alpine

WORKDIR /usr/src/app

RUN apk add wget

COPY ./main.sh .

ENTRYPOINT [./main.sh]
